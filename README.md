# PyEd-Payroll

PyEd-Payroll generates LAUGH-compliant journal entries from ADP-created payroll reports in order to efficiently assign account codes to the payroll data in any accounting system.

## Compiling

To compile on any OS, you will need PyInstaller.

Using PIP:

	$ pip3 install pyinstaller

### Linux
Execute "linux_compile_pyinstaller.sh". The binary will be in the "dist" folder.

### Windows
Execute "windows_compile_pyinstaller.bat". (Untested)
