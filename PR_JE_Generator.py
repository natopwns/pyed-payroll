#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#	PR_JE_Generator.py
#
#	Copyright 2018 Nathan Taylor
#
#	This file is part of "PyEd-Payroll".
#
#	"PyEd-Payroll" is free software: you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation, either version 3 of the License, or (at your option)
#	any later version.
#
#	"PyEd-Payroll" is distributed in the hope that it will be useful, but
#	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#	or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#	for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with "PyEd-Payroll".  If not, see: <https://www.gnu.org/licenses/>.

# DESCRIPTION:
#	This file contains the main instructions for the program.

import tkinter as tk
from tkinter.filedialog import askopenfilename
import csv
import Payroll_Formatting as pf
import JE_Formatting as je


# main class
class MainApp:
	def __init__(self, master):
		# create class-level variable that controls the master tk window
		self.master = master
		master.title("PyEd-Payroll v1.0")
		master.resizable(False, False)
		master.configure(bg = '#6E232E')
		
		# other class-level variables
		self.fileName = ''
		self.payrollReport = ''
		self.organizedList = ''
		self.journalEntry = ''
		self.prMonth = ''
		self.prDay = ''
		self.prYear = ''
		self.outputText = tk.StringVar()
		self.outputText.set('Ready.')
		self.formatList = ('ADP', 'Other (Not Implemented Yet)')
		self.selectedFormat = tk.StringVar()
		self.selectedFormat.set(self.formatList[0])
		self.textColor = 'white'
		self.bgColor = '#6E232E'
		self.buttonColor = '#424242'
		self.aButtonColor = '#515151'
		self.buttonRelief = 'flat'
		self.tkGridColumns = 12
		
		# format dropdown label
		# ---
		self.lblTitle = tk.Label(
			master,
			text = 'Payroll Report Format:',
			bg = self.bgColor,
			fg = self.textColor
			)
		self.lblTitle.grid(
			pady = 1,
			columnspan = self.tkGridColumns,
			row = 0,
			column = 0
			)
		
		# format dropdown box
		# ---
		self.mnuFormat = tk.OptionMenu(
			master,
			self.selectedFormat,
			*self.formatList
			)
		self.mnuFormat.config(
			bd = 0,
			bg = self.buttonColor,
			fg = self.textColor,
			activebackground = self.aButtonColor,
			activeforeground = self.textColor,
			relief = self.buttonRelief
			)
		self.mnuFormat['menu'].config(
			bg = self.buttonColor,
			fg = self.textColor,
			activebackground = self.aButtonColor,
			activeforeground = self.textColor,
			relief = self.buttonRelief
			)
		self.mnuFormat.grid(
			padx = 8,
			pady = 4,
			columnspan = self.tkGridColumns,
			row = 1,
			column = 0
			)
		
		# month entry label
		# ---
		self.lblMonth = tk.Label(
			master,
			text = 'MM:',
			bg = self.bgColor,
			fg = self.textColor
			)
		self.lblMonth.grid(
			row = 2,
			column = 6,
			sticky = 'E'
			)
		
		# month entry box
		# ---
		self.txtMonth = tk.Entry(
			master,
			width = 2,
			relief = self.buttonRelief,
			bg = self.buttonColor,
			fg = self.textColor
			)
		self.txtMonth.grid(
			pady = 4,
			row = 2,
			column = 7,
			sticky = 'W'
			)
		
		# day entry label
		# ---
		self.lblDay = tk.Label(
			master,
			text = 'DD:',
			bg = self.bgColor,
			fg = self.textColor
			)
		self.lblDay.grid(
			row = 2,
			column = 8,
			sticky = 'E'
			)
		
		# day entry box
		# ---
		self.txtDay = tk.Entry(
			master,
			width = 2,
			relief = self.buttonRelief,
			bg = self.buttonColor,
			fg = self.textColor
			)
		self.txtDay.grid(
			pady = 4,
			row = 2,
			column = 9,
			sticky = 'W'
			)
		
		# year entry label
		# ---
		self.lblYear = tk.Label(
			master,
			text = 'YY:',
			bg = self.bgColor,
			fg = self.textColor
			)
		self.lblYear.grid(
			row = 2,
			column = 10,
			sticky = 'E'
			)
		
		# year entry box
		# ---
		self.txtYear = tk.Entry(
			master,
			width = 2,
			relief = self.buttonRelief,
			bg = self.buttonColor,
			fg = self.textColor
			)
		self.txtYear.grid(
			pady = 4,
			row = 2,
			column = 11,
			sticky = 'W'
			)
		
		# organize button
		# ---
		self.btnOrganize = tk.Button(
			master,
			text = 'Organize into List',
			width = 18,
			command = self.organize_list,
			bg = self.buttonColor,
			fg = self.textColor,
			activebackground = self.aButtonColor,
			activeforeground = self.textColor,
			relief = self.buttonRelief
			)
		self.btnOrganize.grid(
			padx = 24,
			pady = 4,
			columnspan = int(self.tkGridColumns/2),
			row = 3,
			column = 0
			)
		
		# generate JE button
		# ---
		self.btnGenerate = tk.Button(
			master,
			text = 'Generate Journal Entry',
			width = 18,
			command = self.generate_journal_entry,
			bg = self.buttonColor,
			fg = self.textColor,
			activebackground = self.aButtonColor,
			activeforeground = self.textColor,
			relief = self.buttonRelief
			)
		self.btnGenerate.grid(
			padx = 24,
			pady = 4,
			columnspan = int(self.tkGridColumns/2),
			row = 3,
			column = int(self.tkGridColumns/2)
			)
		
		# feedback label
		# ---
		self.lblOutput = tk.Label(
			master,
			textvariable = self.outputText,
			bg = self.bgColor,
			fg = self.textColor
			)
		self.lblOutput.grid(
			pady = 10,
			columnspan = self.tkGridColumns,
			row = 4,
			column = 0
			)

	def get_date(self, m, d, y):
		mm = m.get() + '00'
		dd = d.get() + '00'
		yy = y.get() + '00'
		self.prMonth = str(mm)[:2]
		self.prDay = str(dd)[:2]
		self.prYear = str(yy)[:2]

	# load a csv file (csvFile) and store it to a list variable
	def load_csv(self, listChoice):
		# open file dialog
		self.outputText.set('Opening File...')
		csvFile = askopenfilename(title="Select " + listChoice, filetypes=[
			("Comma-separated values","*.csv"),("All files","*.*")
			])
		if (type(csvFile) is tuple
				or csvFile == ''
				or csvFile[csvFile.rfind('.'):len(csvFile)] != '.csv'):
			self.outputText.set('Please choose a CSV file.')
			return False
		else:
			if listChoice == 'Account List':
				pass
			else:
				self.fileName = csvFile[csvFile.rfind('/')+1:len(csvFile)-4]
			# open chosen file
			with open(csvFile, 'r') as importedFile:
				reader = csv.reader(importedFile, delimiter=',', quotechar='"')
				# store file to list
				if listChoice == 'CSV':
					self.payrollReport = list(reader)
				else:
					self.accountList = list(reader)

	def save_list_to_csv(self, vList, fType):
		with open(
				self.fileName + '_' + fType + '.csv','w', newline=''
				) as organizedFile:
			writer = csv.writer(organizedFile, dialect='excel')
			writer.writerows(vList)
			
		self.outputText.set(
			'Saved to: ' + self.fileName + '_' + fType + '.csv'
			)

	# run instructions from pf, and then store the output to a new csv
	def organize_list(self):
		loadCheck = self.load_csv('CSV')
		if loadCheck == False:
			return
		else:
			if self.selectedFormat.get() == 'ADP':
				#\! I can't figure out why I have to tell this function that
				#\! 'self' is 'pf.InstructionsADP()' !#
				self.organizedList = pf.InstructionsADP.organize_list_ADP(
					pf.InstructionsADP(), self.payrollReport
					)
			else:
				self.outputText.set('Not implemented yet.')
				return
			self.save_list_to_csv(self.organizedList, 'organized')
	
	def generate_journal_entry(self):
		self.get_date(self.txtMonth, self.txtDay, self.txtYear)
		loadCheck = self.load_csv('CSV')
		if loadCheck == False:
			return
		else:
			loadCheck = self.load_csv('Account List')
			if loadCheck == False:
				return
			else:
				self.journalEntryList = je.Instructions.convert_to_JE(
					je.Instructions(),
					self.payrollReport,
					self.accountList,
					self.prMonth,
					self.prDay,
					self.prYear
					)
				self.save_list_to_csv(self.journalEntryList, 'journal_entry')

# actual execution of program
root = tk.Tk()
App = MainApp(root)
root.mainloop()
