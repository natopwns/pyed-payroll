#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#	Payroll_Formatting.py
#
#	Copyright 2018 Nathan Taylor
#
#	This file is part of "PyEd-Payroll".
#
#	"PyEd-Payroll" is free software: you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation, either version 3 of the License, or (at your option)
#	any later version.
#
#	"PyEd-Payroll" is distributed in the hope that it will be useful, but
#	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#	or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#	for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with "PyEd-Payroll".  If not, see: <https://www.gnu.org/licenses/>.
#
# DESCRIPTION:
#	This file includes formatting instructions for every currently supported
#	payroll platform.


# class for tempStorage variables (needs to match organizedList)
class AddingVarsADP:
	def __init__(self):
		self.GrossPay = 0
		self.TutorPay = 0
		self.CoachPay = 0
		self.PIP = 0
		self.Reimbursement = 0
		self.DedRem = 0
		self.Bonus = 0
		self.ErSocSec = 0
		self.ErMedi = 0
		self.FIT = 0
		self.SIT = 0
		self.Medical = 0
		self.Life = 0
		self.Dental = 0
		self.Vision = 0
		self.d401k = 0
		self.Accident = 0
		self.Illness = 0
		self.Heart = 0
		self.Cancer = 0
		self.Disability = 0
		self.WageAssignments = 0
		self.Other = 0
		self.Advance = 0
		self.Unemployment = 0
		self.SocialSecurity = 0
		self.Medicare = 0
		self.NetPay = 0

class InstructionsADP:
	def __init__(self):
		
		# class-level variables
		# list for csv (needs to match AddingVarsADP)
		self.organizedList = [[
			'Name',
			'GrossPay',
			'TutorPay',
			'CoachPay',
			'PIP',
			'Reimbursement',
			'DedRem',
			'Bonus',
			'ErSocSec',
			'ErMedi',
			'FIT',
			'SIT',
			'Medical',
			'Life',
			'Dental',
			'Vision',
			'd401k',
			'Accident',
			'Illness',
			'Heart',
			'Cancer',
			'Disability',
			'WageAssignments',
			'Other',
			'Advance',
			'Unemployment',
			'SocialSecurity',
			'Medicare',
			'NetPay'
			]]
		
		# format: [column# name][column# amount][name of new column]
		self.payrollDict = {
			'1099 Misc': '0003GrossPay',
			'Regular': '0003GrossPay',
			'Final Paycheck': '0003GrossPay',
			'Tutor': '0003TutorPay',
			'Coaching Pay': '0003CoachPay',
			'PIP Pay': '0003PIP',
			'Expense reimbursement non-taxable': '0003Reimbursement',
			'1099 reimbursement': '0003Reimbursement',
			'Deduction Reimbursement': '0003DedRem',
			'Bonus': '0003Bonus',
			'FED SOCSEC': '0405SocialSecurity',
			'FED MEDCARE': '0405Medicare',
			'FED FIT': '0405FIT',
			'LA SIT': '0405SIT',
			'Blue Cross (pre-tax) $': '0607Medical',
			'Life insurance': '0607Life',
			'Dental pre-tax': '0607Dental',
			'Vision pre-tax': '0607Vision',
			'401(k) plan $': '0607d401k',
			'401(k) plan %': '0607d401k',
			'Accident_Supplemental': '0607Accident',
			'Critical Illness_Supplemental': '0607Illness',
			'Heart and Stroke_Supplemental': '0607Heart',
			'Cancer_Supplemental': '0607Cancer',
			'Short-term disability $': '0607Disability',
			'Creditor 1': '0607WageAssignments',
			'Child support 1': '0607WageAssignments',
			'Employer processing fee': '0607Other',
			'Advance': '0607Advance',
			'LA SUI-ER': '0910Unemployment',
			'FED SOCSEC-ER': '0910ErSocSec',
			'FED MEDCARE-ER': '0910ErMedi',
			'NetPay': '0808NetPay'
			}

		print('---\nUsing ADP Instructions\n---')

	# check for non-numeric characters in numbers
	def val_err(self, num):
		try:
			float(num)
		except ValueError:
			num = num.replace(',', '').replace('$', '')
		return float(num)

	def organize_list_ADP(self, payrollReport):
		
		newRow = 0
			
		for row in payrollReport:
			
			if (row[0][:10] == 'Employee: '
				or row[0][:20] == 'Pay Frequency Totals'
				or row[0] == 'Company Totals:'):

				# store the adding variables from the previous employee
				if newRow == 0:
					pass
				else:
					for column in self.organizedList[0]:
						if column == 'Name':
							pass
						else:
							self.organizedList[newRow].append(
								eval('tempStorage.' + column))
				
				# reset the adding variables
				tempStorage = AddingVarsADP()
				
				# add a new row for the next employee
				if row[0][:10] == 'Employee: ':
					self.organizedList.append([row[0][10:]])
				else:
					self.organizedList.append(['TOTALS:'])
				newRow += 1
			
			# assign values from payrollReport to tempStorage according to
			# payrollDict
			for k, v in self.payrollDict.items():
				
				fieldName = v[4:]
				numRowDes = int(v[0:2])
				numRowVal = int(v[2:4])
				
				if row[numRowDes] == k:
					setattr(tempStorage, fieldName,
							getattr(tempStorage, fieldName)
							+ self.val_err(row[numRowVal]))
				# check for net pay value
				if fieldName == 'NetPay':
					numNet = row[numRowVal]
					try:
						numNet = float(numNet)
						if numNet > 0.0:
							setattr(tempStorage, fieldName,
								getattr(tempStorage, fieldName)
								+ self.val_err(row[numRowVal]))
					except ValueError:
						pass

		return self.organizedList
		
