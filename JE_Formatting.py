#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#	JE_Formatting.py
#
#	Copyright 2018 Nathan Taylor
#
#	This file is part of "PyEd-Payroll".
#
#	"PyEd-Payroll" is free software: you can redistribute it and/or modify it
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation, either version 3 of the License, or (at your option)
#	any later version.
#
#	"PyEd-Payroll" is distributed in the hope that it will be useful, but
#	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#	or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#	for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with "PyEd-Payroll".  If not, see: <https://www.gnu.org/licenses/>.
#
# DESCRIPTION:
#	This file includes formatting instructions for the journal entry

class Instructions:
	def __init__(self):
		# specify account codes for each column
		self.dctCodings = {
			'GrossPay': ['debit', 'empAcct', 'empFund', True],
			'CoachPay': ['debit', '1501420', '1 - General Fund', True],
			'PIP': ['debit', '1121497', '1 - General Fund', True],
			'Reimbursement': ['debit', '3392830', '1 - General Fund', True],
			'DedRem': ['debit', 'empAcct', 'empFund', True],
			'Bonus': ['debit', '1501495', '1 - General Fund', True],
			'TutorPay': ['debit', '1501490', '1 - General Fund', True],
			'ErSocSec': ['debit', '220', 'empFund', False],
			'ErMedi': ['debit', '225', 'empFund', False],
			'FIT': ['credit', '4610000', '1 - General Fund', False],
			'SIT': ['credit', '4610000', '1 - General Fund', False],
			'Medical': ['credit', '210', 'empFund', False],
			'Life': ['credit', '4710000', '1 - General Fund', False],
			'Dental': ['credit', '4710000', '1 - General Fund', False],
			'Vision': ['credit', '4710000', '1 - General Fund', False],
			'd401k': ['credit', '239', 'empFund', False],
			'Accident': ['credit', '4710000', '1 - General Fund', False],
			'Illness': ['credit', '4710000', '1 - General Fund', False],
			'Heart': ['credit', '4710000', '1 - General Fund', False],
			'Cancer': ['credit', '4710000', '1 - General Fund', False],
			'Disability': ['credit', '4710000', '1 - General Fund', False],
			'WageAssignments': ['credit', '4710000', '1 - General Fund', False],
			'Other': ['credit', '4710000', '1 - General Fund', False],
			'Advance': ['credit', '4710000', '1 - General Fund', False],
			'Unemployment': ['debit', '250', 'empFund', False],
			'SocialSecurity': ['credit', '4610000', '1 - General Fund', False],
			'Medicare': ['credit', '4610000', '1 - General Fund', False],
			'NetPay': ['credit', '4610000', '1 - General Fund', False]
			}
		# initialize the journal entry list
		self.journalEntry = [[
			'JrnlNo',
			'JrnlDate',
			'Memo',
			'AcctNo',
			'Debit',
			'Credit',
			'Description',
			'Class'
			]]
		print('---\nInitialization Complete\n---')

	def add_to_dict(self, num, dctVar, dctVal):
		dctVar[dctVal] = (
			round(float(dctVar[dctVal]), 2)
				+ round(float(num), 2)
			)

	def calc_ss_medi(self, num, dctVar, total, jN, jD, m, aN, d, fN):
		for k, v in dctVar.items():
			if v == 0:
				pass
			else:
				portion = float(v)/total
				debit = round(float(portion) * float(num), 2)
				if k == 'GrossPay':
					fund = fN
				else:
					fund = '1 - General Fund'
				self.journalEntry.append([jN, jD, m, aN, debit, 0, d, fund])

	def balance_check(self, balance, dctBalance, num, dctTotals):
		if balance in dctBalance:
			dctBalance[balance] = num
			self.add_to_dict(num, dctTotals, balance)

	# convert an organized csv to a journal entry format
	def convert_to_JE(self, lstOrg, lstAcct, mm, dd, yy):
		# initialize top-level variables
		date = yy + mm + dd
		jrnlNo = 'PR ' + date
		jrnlDate = date[2:4] + '/' + date[4:] + '/' + date[:2]
		memo = 'Payroll Dated ' + jrnlDate
		bin461 = 0.0
		bin471 = 0.0
		# vars for 461 and 471 calc
		dctTotals = {
			'debit': 0,
			'credit': 0
			}
		# loop through the rows in lstOrg
		for rowCount, row in enumerate(lstOrg):
			# skip the first and last row in lstOrg
			if rowCount == 0 or row[0] == 'TOTALS:':
				pass
			else:
				# initialize row-level variables
				empName = row[0]
				description = empName + ' ' + memo
				empAcct = 'NO_CODE_FOUND'
				empFund = 'NO_FUND_FOUND'
				# find empName's account from lstAcct
				for rowAcct in lstAcct:
					if rowAcct[0] == empName:
						empAcct = rowAcct[1]
						empFund = rowAcct[2]
						break
				acctNo = empAcct
				fundNo = empFund
				# vars for SocSec and Medicare calc
				totalPay = 0.0
				dctSSMedi = {
					'GrossPay': 0,
					'CoachPay': 0,
					'PIP': 0,
					'Reimbursement': 0,
					'DedRem': 0,
					'Bonus': 0,
					'TutorPay': 0
					}
				# loop through the columns in the current row
				for numCount, num in enumerate(row):
					colHead = lstOrg[0][numCount]
					# skip the first column in the row
					if colHead == 'Name' or float(num) == 0:
						pass
					else:
						dctBalance = {
							'debit': 0,
							'credit': 0
							}
						# determine account for current line
						if colHead in self.dctCodings:
							balance = self.dctCodings[colHead][0]
							acctNo = self.dctCodings[colHead][1]
							fundNo = self.dctCodings[colHead][2]
							# check if line item is a debit or credit
							self.balance_check(balance, dctBalance, num,
								dctTotals)
							# update bin461 and bin471
							if acctNo == '4610000':
								bin461 += float(num)
								continue
							elif acctNo == '4710000':
								bin471 += float(num)
								continue
							# mirror in bin461
							if (colHead == 'Unemployment'
											or colHead == 'ErSocSec'
											or colHead == 'ErMedi'):
								# mirror in 461 and totals
								bin461 += float(num)
								self.add_to_dict(num, dctTotals, 'credit')
							# check if acctNo needs to be the main empAcct
							if acctNo == 'empAcct':
								acctNo = empAcct
							# check if acctNo needs a function code
							elif len(acctNo) == 3:
								acctNo += empAcct[3:]
							# check if fundNo needs to be the main empFund
							if fundNo == 'empFund':
								fundNo = empFund
							# check if num is part of SocSec and Medi calc
							if self.dctCodings[colHead][3] == True:
								totalPay += float(num)
								self.add_to_dict(num, dctSSMedi, colHead)
							# calc SocSec and Medicare, and write JE line
							if (colHead == 'ErSocSec'
											or colHead == 'ErMedi'):
								self.calc_ss_medi(num, dctSSMedi, totalPay,
													jrnlNo, jrnlDate, memo,
													acctNo, description, fundNo)
							else:
								# append current line item to next line of JE
								self.journalEntry.append([
									jrnlNo, jrnlDate, memo, acctNo,
									round(float(dctBalance['debit']), 2),
									round(float(dctBalance['credit']), 2),
									description, fundNo
									])
		# append bin461 to next line of JE
		self.journalEntry.append([
			jrnlNo, jrnlDate, memo, '4610000', 0, round(bin461, 2), memo,
			'1 - General Fund'
			])
		# append bin471 to last line of JE
		self.journalEntry.append([
			jrnlNo, jrnlDate, memo, '4710000', 0, round(bin471, 2), memo,
			'1 - General Fund'
			])
		# balance transaction (if not equal)
		dctTotals['credit']
		balTotal = float(dctTotals['debit']) - float(dctTotals['credit'])
		if balTotal > 0:
			self.journalEntry.append([
				jrnlNo, jrnlDate, memo, '2201110', 0, round(balTotal, 2), memo,
				'1 - General Fund'
				])
		elif balTotal < 0:
			self.journalEntry.append([
				jrnlNo, jrnlDate, memo, '2201110', round(-balTotal, 2), 0, memo,
				'1 - General Fund'
				])
		return self.journalEntry
